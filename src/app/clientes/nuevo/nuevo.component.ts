import { Component, OnInit } from '@angular/core';
import { MyService } from "./../../services/my.service";
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert2'

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {

  constructor( private api:MyService,private activatedRoute: ActivatedRoute,private router:Router, private location: Location) { }

  name:string;
  first_lastname:string;
  second_lastname:string;
  rfc:string;
  idClient:string;
  idClientt:number;
  isUpdated:boolean=false;
  isValid:boolean=true;

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      let id = params['id'];
      if(id){
        this.isUpdated = true;
        this.api.geteClientDetail(id)
          .subscribe(response =>{              
            this.name = response.nombre;
            this.first_lastname = response.first_lastname;
            this.second_lastname = response.second_lastname;
            this.rfc = response.rfc;
            this.idClientt = id;
            this.parseClave();
            this.isValidForm();
          });
      }else{
        this.api.get_clave_clientes()
          .subscribe(response =>{this.idClientt=response.Auto_increment; this.parseClave()})
      }
    });
  }

  parseClave(){
    var str = "" + this.idClientt
    var pad = "0000"
    this.idClient = pad.substring(0, pad.length - str.length) + str
  }

  saveClient(){
    if(this.isUpdated){
      this.api.updatedClient(this.idClientt,this.name,this.first_lastname,this.second_lastname,this.rfc)
        .subscribe(response =>{
          if(response == true){
            this.location.replaceState('/');
            this.router.navigate(['/clientes']);
            swal("Bien hecho","La Informacion ha sido actualizada correctamente","success");
          }
        })
    }else{
      this.api.createClient(this.name,this.first_lastname,this.second_lastname,this.rfc)
      .subscribe(response => {
        if(response == true){
          this.location.replaceState('/');
          this.router.navigate(['/clientes']);
          swal("Bien Hecho","El cliente ha sido registrado correctamente","success");
        }
      })
    }
  }

  isValidForm() {
    if( this.name && this.first_lastname && this.second_lastname && this.rfc ){
      this.isValid = false;
    }else { 
      this.isValid = true;
    }
}

  cancel(){
    swal({
      title: 'Cancelar',
      text: "¿Desea Abandonar la Pantalla Actual?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then(function() {
      window.location.href = "/clientes";
    })
  }

}
