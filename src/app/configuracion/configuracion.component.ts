import { Component, OnInit } from '@angular/core';
import { MyService } from "./../services/my.service";
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert2'

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {

  constructor(private api:MyService,private activatedRoute: ActivatedRoute,private router:Router, private location: Location) { }

  tasa:number=0;
  enganche:number=0;
  plazo:number=0;

  isValid:boolean=true;
  
    ngOnInit() {
      this.api.getConfiguration()
        .subscribe(response =>{
          this.tasa = response.financiamiento;
          this.enganche = response.enganche;
          this.plazo = response.plazo;
          this.isValidForm();
        });
    }
  
  
    saveData(){
      this.api.updatedConfiguration(this.tasa,this.enganche,this.plazo)
        .subscribe(response => {
          if(response){
            this.location.replaceState('/');
            this.router.navigate(['/ventas']);
            swal("Bien Hecho","La Configuracion se ha almacenado Correctamente","success");
          }
        })
    }
  
    isValidForm() {
      if( this.tasa && this.enganche && this.plazo ){
        this.isValid = false;
      }else { 
        this.isValid = true;
      }
  }
  
    cancel(){
      swal({
        title: 'Cancelar',
        text: "¿Desea Abandonar la Pantalla Actual?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
      }).then(function() {
        window.location.href = "/ventas";
      })
    }

}
