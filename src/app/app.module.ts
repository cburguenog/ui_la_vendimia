import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

//Services
import { MyService } from './services/my.service';

//thirt party libraries
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

//Routing 
import { routing,appRoutingProvides } from './app.routing';
//componentes 
import { AppComponent } from './app.component';
import { VentasComponent } from './ventas/ventas.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { NuevoComponent } from './clientes/nuevo/nuevo.component';
import { FormularioArticuloComponent } from './articulos/formulario-articulo/formulario-articulo.component';
import { FormularioVentasComponent } from './ventas/formulario-ventas/formulario-ventas.component';

@NgModule({
  declarations: [
    AppComponent,
    VentasComponent,
    ClientesComponent,
    ArticulosComponent,
    ConfiguracionComponent,
    NuevoComponent,
    FormularioArticuloComponent,
    FormularioVentasComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    FormsModule,
    NguiAutoCompleteModule
  ],
  providers: [appRoutingProvides,MyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
