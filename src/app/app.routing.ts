import { Routes, RouterModule } from '@angular/router';
import { VentasComponent } from './ventas/ventas.component';
import { FormularioVentasComponent } from "./ventas/formulario-ventas/formulario-ventas.component";
import { ClientesComponent } from './clientes/clientes.component';
import { NuevoComponent } from "./clientes/nuevo/nuevo.component";
import { ArticulosComponent } from "./articulos/articulos.component";
import { FormularioArticuloComponent } from "./articulos/formulario-articulo/formulario-articulo.component";
import { ConfiguracionComponent } from "./configuracion/configuracion.component";


const appRoutes: Routes=[
  { path:'ventas',component:VentasComponent },
  { path:'ventas/nueva-venta',component:FormularioVentasComponent },
  { path:'ventas/detalle/:id',component:FormularioVentasComponent },
  { path:'clientes',component:ClientesComponent },
  { path:'clientes/nuevo-cliente',component:NuevoComponent },
  { path:'clientes/editar/:id', component: NuevoComponent },
  { path:'articulos',component:ArticulosComponent },
  { path:'articulos/nuevo-articulo',component:FormularioArticuloComponent },
  { path:'articulos/editar/:id',component:FormularioArticuloComponent },
  { path:'configuracion',component:ConfiguracionComponent }
];

export const appRoutingProvides: any[]=[];
export const routing=RouterModule.forRoot(appRoutes);