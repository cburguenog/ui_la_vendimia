import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'app';
  newDate = new Date().toLocaleDateString("MX");
  date= this.newDate.split('/')[2] +"-" + this.newDate.split('/')[1] +"-" + this.newDate.split('/')[0];
}
