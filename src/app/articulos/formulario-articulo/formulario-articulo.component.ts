import { Component, OnInit } from '@angular/core';
import { MyService } from "./../../services/my.service";
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert2'

@Component({
  selector: 'app-formulario-articulo',
  templateUrl: './formulario-articulo.component.html',
  styleUrls: ['./formulario-articulo.component.css']
})
export class FormularioArticuloComponent implements OnInit {

  description:string;
  model:string;
  price:number;
  existence:number;
  itemID:number;
  itemid:string;

  isUpdate:boolean=false;
  isValid:boolean=true;

  constructor(private api:MyService,private activatedRoute: ActivatedRoute,private router:Router, private location: Location) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      let id = params['id'];
      if(id){
        this.isUpdate = true;  
        this.api.getItemDetail(id)
          .subscribe(response =>{              
            this.description = response.description;
            this.model = response.model;
            this.price = response.price;
            this.existence = response.existence;
            this.itemID = id;
            this.parseClave();
            this.isValidForm();
          });
      }else{
        this.api.get_clave_articulos()
          .subscribe(response =>{this.itemID=response.Auto_increment; this.parseClave()})
      }
    });
  }

  parseClave(){
    var str = "" + this.itemID
    var pad = "0000"
    this.itemid = pad.substring(0, pad.length - str.length) + str
  }

  saveItem(){
    if(this.isUpdate){
      this.api.updatedItem(this.itemID,this.description,this.model,this.price,this.existence)
        .subscribe(response =>{
          if(response == true){
            this.location.replaceState('/');
            this.router.navigate(['/articulos']);
            swal("Bien hecho","La Informacion ha sido actualizada correctamente","success");
          }
        })
    }else{
      this.api.createItem(this.description,this.model,this.price,this.existence)
      .subscribe(response => {
        if(response == true){
          this.location.replaceState('/');
          this.router.navigate(['/articulos']);
          swal("Bien Hecho","El Articulo ha sido registrado correctamente","success");
        }
      })
    }
  }

  isValidForm() {
    if( this.description && this.price && this.existence >=0){
      this.isValid = false;
    }else { 
      this.isValid = true;
    }
}

  cancel(){
    swal({
      title: 'Cancelar',
      text: "¿Desea Abandonar la Pantalla Actual?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then(function() {
      window.location.href = "/articulos";
    })
  }

}
