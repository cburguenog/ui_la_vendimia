import { Component, OnInit } from '@angular/core';
import { MyService } from "./../services/my.service";
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';


//jQuery
declare var $:any;

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.css']
})
export class ArticulosComponent implements OnInit {

  data =[];
  show = false;

  constructor(private api:MyService,private activatedRoute: ActivatedRoute,private router:Router, private location: Location) { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    this.api.getItems()
      .subscribe((response) => {
        $.getScript('../../assets/js/jquery.dataTables.min.js',function(){
            $('#datatable').DataTable(
                {"language": {"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"}}
            );
        });
        this.data = response;
        this.show = true;
      })
  }

}
