import { Injectable } from '@angular/core';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import swal from 'sweetalert2'

@Injectable()
export class MyService {

  constructor(private http:Http) { }

  host:string ="http://45.55.14.181:7000";

  getClientes():Observable<any>{
    let url = this.host + `/clientes/`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  createClient(name:string, first_lastname:string, second_lastname:string,rfc:string):Observable<boolean>{
    let url = this.host + `/clientes/new_client`;
    return this.http.post(url,{name,first_lastname,second_lastname,rfc})
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return true; }
          else if (status== 405){ swal("No es posible continuar",' Por Favor Completa el Formulario antes de enviar los datos', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      })
  }

  buscarClientes(param):Observable<any>{
    let url = this.host + `/clientes/buscar/${param}`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  geteClientDetail(id):Observable<any>{
    let url = this.host + `/clientes/${id}`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("No es posible continuar",'Por Favor Completa el Formulario antes de enviar los datos', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  updatedClient(id:number, name:string, first_lastname:string, second_lastname:string,rfc:string):Observable<boolean>{
    let url = this.host + `/clientes/${id}/update`;
    return this.http.put(url,{name,first_lastname,second_lastname,rfc})
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return true; }
          else if (status== 405){ swal("No es posible continuar",' Por Favor Completa el Formulario antes de enviar los datos', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      })
  }

  getItems():Observable<any>{
    let url = this.host + `/articulos/`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  createItem(description:string,model:string,price:number,existence:number):Observable<any>{
    let url = this.host + `/articulos/new_item`;
    return this.http.post(url,{description,model,price,existence})
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return true; }
          else if (status== 405){ swal("No es posible continuar",'Por Favor Completa el Formulario antes de enviar los datos', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      })
  }

  getItemDetail(id):Observable<any>{
    let url = this.host + `/articulos/${id}`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("No es posible continuar",'Por Favor Completa el Formulario antes de enviar los datos', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  updatedItem(id:number, description:string,model:string,price:number,existence:number):Observable<boolean>{
    let url = this.host + `/articulos/${id}/update`;
    return this.http.put(url,{description,model,price,existence})
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return true; }
          else if (status== 405){ swal("No es posible continuar",' Por Favor Completa el Formulario antes de enviar los datos', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      })
  }

  getConfiguration():Observable<any>{
    let url = this.host + `/configuracion/`;
    return this.http.get(url)
      .map((response:Response)=>{
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      })
  }

  updatedConfiguration(financiamiento,enganche,plazo):Observable<any>{
    let url = this.host + `/configuracion/update`;
    return this.http.put(url,{financiamiento,enganche,plazo})
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return true; }
          else if (status== 405){ swal("No es posible continuar",' Por Favor Completa el Formulario antes de enviar los datos', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      })
  }

  getVentas():Observable<any>{
    let url = this.host + `/ventas/`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  create_venta(id_cliente:number,total:number,plazo:number,lista):Observable<any>{
    let url = this.host + `/ventas/nueva_venta`;
    return this.http.post(url,{id_cliente,total,plazo,lista})
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return true; }
          else if (status== 405){ swal("No es posible continuar",'Por Favor Completa el Formulario antes de enviar los datos', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      })
  }

  get_clave_ventas():Observable<any>{
    let url = this.host + `/ventas/obtener_clave`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  get_clave_clientes():Observable<any>{
    let url = this.host + `/clientes/obtener_clave`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  get_clave_articulos():Observable<any>{
    let url = this.host + `/articulos/obtener_clave`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }

  buscarArticulos(param):Observable<any>{
    let url = this.host + `/articulos/buscar/${param}`;
    return this.http.get(url)
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){ return response.json().results; }
          else if (status== 405){ swal("Error",'Ha ocurrido un error en el proceso. intentelo mas tarde', "error"); }
            else { swal("Error",'Ha Ocurrido un error en el servidor', "error"); }
      });
  }
}
