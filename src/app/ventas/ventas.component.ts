import { Component, OnInit } from '@angular/core';
import { MyService } from "./../services/my.service";
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';

//jQuery
declare var $:any;

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  data =[];

  constructor(private api:MyService,private activatedRoute: ActivatedRoute,private router:Router, private location: Location) { }

  ngOnInit() {
    this.madeTable();
  }


  madeTable(){
    this.api.getVentas()
      .subscribe(response =>{
        $.getScript('../../assets/js/jquery.dataTables.min.js',function(){
          $('#datatable').DataTable(
              {"language": {"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"}}
          );
      });
      this.data = response;
      })
  }

}
