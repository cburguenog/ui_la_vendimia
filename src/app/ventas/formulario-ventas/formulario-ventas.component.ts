import { Component, OnInit } from '@angular/core';
import { MyService } from "./../../services/my.service";
import { Router, ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert2'

@Component({
  selector: 'app-formulario-ventas',
  templateUrl: './formulario-ventas.component.html',
  styleUrls: ['./formulario-ventas.component.css']
})
export class FormularioVentasComponent implements OnInit {

  constructor(private api:MyService,private activatedRoute: ActivatedRoute,private router:Router, private location: Location) { }

  //auto-complete clientes;
  param:string="";
  name_list:Array<any> = [];
  client:any;
  idClient:number;
  rfc:string;

  //configuracion de credito;
  config:any;

  //totales
  enganche:any = 0.00;
  bono_enganche:any=0.00;
  subtotal:any;
  total_adeudo:any=0.00;

  //auto-complete articulos
  item_list:Array<any> = [];
  item:any;
  item_list_selected:Array<any>=[];

  //plazos
  plazos:Array<number> = [3,6,9,12];
  plazo_seleccionado:number=0;

  //Plan
  planes_de_pagos:Array<any> = [];

  idShow:string;
  id:number;

  show:boolean=false;

  ngOnInit() {
    this.obtenerConfiguracion();
    this.activatedRoute.params.subscribe((params: Params) => {
      let id = params['id'];
      if(id){
      }else{
        this.api.get_clave_ventas()
          .subscribe(response => {this.id = response.Auto_increment; this.parseClave(); this.search_clients(); this.searchItem() })
      }
    });
  }

  obtenerConfiguracion(){
    this.api.getConfiguration()
    .subscribe(response => this.config = response);
  }

  parseClave(){
    var str = "" + this.id;
    var pad = "0000";
    this.idShow = pad.substring(0, pad.length - str.length) + str;
  }

  search_clients(){
    this.api.buscarClientes('*')
    .subscribe(response => {
      this.name_list = response;
    })
  }

  selectClient(){
    let aux = this.client.id + '  --  ' + this.client.value;
    this.idClient = this.client.id;
    this.rfc = this.client.rfc;
    this.client = aux;
  }

  searchItem(){
    this.api.buscarArticulos('*')
    .subscribe(response => {
      this.item_list = response;
    })
  }

  addItem(){
    if(this.item.existence > 0){
      let precio_con_intereses = this.item.price * (1 + ((this.config.financiamiento * this.config.plazo) / 100) );
      let item_selected = {
        id:this.item.id,
        value:this.item.value,
        model:this.item.model,
        cantidad:1,
        inventario:this.item.existence,
        price:Math.round(precio_con_intereses),
        importe: Math.round(precio_con_intereses)
      }
      this.item_list_selected.push(item_selected);
      this.item = "";
      this.obtenerSubtotales();
    }else{
      swal({
        title: 'Error',
        text: "El Articulo Seleccionado No Cuenta Con Existencia. Favor de verificar.",
        type: 'warning',
        showCancelButton: true,
        showConfirmButton:false,
        cancelButtonColor: '#3085d6',
        cancelButtonText: 'Aceptar'
      })
    }
  }

  calcularImporte(index){
    if(this.item_list_selected[index].cantidad > this.item_list_selected[index].inventario){
      swal({
        title: 'Advertencia',
        text: "La Cantidad Ingresada Supera el Inventario Actual.",
        type: 'warning',
        showCancelButton: true,
        showConfirmButton:false,
        cancelButtonColor: '#3085d6',
        cancelButtonText: 'Aceptar'
      })
      this.item_list_selected[index].cantidad = this.item_list_selected[index].inventario;
    }{
      this.item_list_selected[index].importe = this.item_list_selected[index].price * this.item_list_selected[index].cantidad;      
    }
    this.obtenerSubtotales();
  }

  obtenerSubtotales(){
    this.enganche = 0.0;
    this.bono_enganche =0.0;
    this.total_adeudo = 0.0;
    this.subtotal = 0.0
    for (var i = 0; i < this.item_list_selected.length; i++) {
      console.log("si entro");
      
      this.subtotal = this.item_list_selected[i].importe;
    }

    this.enganche = ((this.config.enganche/100) * this.subtotal).toFixed(2);
    this.bono_enganche = (this.enganche * ( (this.config.financiamiento * this.config.plazo)/100 ) ).toFixed(2);
    this.total_adeudo = (this.subtotal - this.enganche - this.bono_enganche).toFixed(2);
  }

  cancel(){
    swal({
      title: 'Cancelar',
      text: "¿Desea Abandonar la Pantalla Actual?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then(function() {
      window.location.href = "/ventas";
    })
  }

  next(){
    let flag=true;
    for (var i = 0; i < this.item_list_selected.length; i++) {
       if(this.item_list_selected[i].cantidad <= 0){
         flag = false;
       }
    }
    if(this.idClient && this.item_list_selected.length > 0 && flag){
      this.show = true;
      this.calcularPlazos();
    }else{
      swal({
        title: 'Error',
        text: "Los Datos Ingresados No Son Correctos. Favor de verificar.",
        type: 'warning',
        showCancelButton: true,
        showConfirmButton:false,
        cancelButtonColor: '#3085d6',
        cancelButtonText: 'Aceptar'
      })
    }
  }

  calcularPlazos(){
    let contado =this.total_adeudo /( 1+ (( this.config.financiamiento * this.config.plazo)/100));
    console.log(contado);
    
    let total_a_pagar = this.total_adeudo * (1+ (this.config.financiamiento * this.config.plazo)/100)
    for (var i = 0; i < this.plazos.length; i++) {
      let total_a_pagar = (contado * (1+ (this.config.financiamiento * this.plazos[i])/100));
      let abono = total_a_pagar / this.plazos[i];
      let ahorra = this.total_adeudo - total_a_pagar;
      let item ={ pagar:total_a_pagar.toFixed(2), abono:abono.toFixed(2), ahorra:ahorra.toFixed(2) }

      this.planes_de_pagos.push(item);
    }
  }

  select(index){
    this.plazo_seleccionado = index;
  }

  Guardar(){
    if(this.plazo_seleccionado==0){
      swal({
        title: 'Error',
        text: "Debe seleccionar un plazo para realizar el pago de su compra",
        type: 'warning',
        showCancelButton: true,
        showConfirmButton:false,
        cancelButtonColor: '#3085d6',
        cancelButtonText: 'Aceptar'
      })
    }else{
      this.api.create_venta(this.idClient,this.planes_de_pagos[this.plazo_seleccionado].pagar,this.plazos[this.plazo_seleccionado],this.item_list_selected)
        .subscribe(response=>{
          if(response){
            this.location.replaceState('/');
            this.router.navigate(['/ventas']);
            swal("Bien Hecho","Tu venta ha sido registrada correctamente","success");
          }
        })
    }
  }

  delete(index){
    this.item_list_selected.splice(index,1);
    this.obtenerSubtotales();
  }

}
